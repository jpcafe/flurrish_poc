export const generateNewGame = (startTime, question) => ({
    question,
    answer: [],
    startTime,
    endTime: null
});

export const getTime = () => {
    const date = new Date();
    return date.getTime();
};

export const hasNextQuestion = (index, array = []) => {
    return index < (array.length - 1);
};


export const updateAnswer = (game, key) => {
    return Object.assign({}, game, {answer: game.answer + key});
};

export const deleteAnswer = (game) => {
    return Object.assign({}, game, {answer: game.answer.slice(0, -1)});
};

export const getDefaultGame = () => ({
    startTime: null,
    endTime: null,
    question: [],
    answer: []
});

export const isAnswerCorrect = (question, answer) => {
    return (question[0] * question[1]) === parseInt(answer);
};
