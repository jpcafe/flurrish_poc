import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';

export default class Result extends Component {
    render() {
        const {game, correct} = this.props;
        // <Text>Duration: {this.getDuration(game.startTime, game.endTime)}</Text>

        return (
            <View style={{
                flex: 1,
                marginTop: 10,
                flexDirection: 'row',
                alignItems: 'center'
            }}>
                {this.renderImage(correct)}
                <Text style={{
                    fontSize: 30,
                    marginLeft: 20,
                    width: 150,
                    color: '#575757',
                }}>{game.question[0]} x {game.question[1]} = {game.answer}</Text>
                <Text style={{
                    fontSize: 22,
                    marginLeft: 20,
                    color: '#575757',
                }}>({this.getDuration()} s)</Text>
            </View>
        );
    }

    renderImage(correct) {
        if (correct) {
            return (<View style={{
                borderRadius: 4,
                borderWidth: 0.8
            }}><Image source={require('./../assets/done.png')}/></View>);
        } else {
            return (<View style={{
                borderRadius: 4,
                borderWidth: 0.8
            }}><Image source={require('./../assets/clear.png')}/></View>);
        }
    }

    getDuration() {
        return (this.props.game.endTime - this.props.game.startTime) / 1000;
    }
}
