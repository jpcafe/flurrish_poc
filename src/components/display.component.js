import React from 'react';
import {Text, View} from 'react-native';

const sty = ({});
export default ({question, answer}) => {
    return (
        <View style={{
            height: 100,
            marginLeft: 30,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center'
        }}>
            <Text style={{color:'#3d3d3d', fontSize: 45, width: 160}}>{question[0]} x {question[1]} =</Text>
            <Text style={{
                color:'#3d3d3d', textAlign: 'center', fontSize: 45, width: 120, height: 65, borderRadius: 4,
                borderWidth: 1
            }}>{answer}</Text>
        </View>
    )
};
