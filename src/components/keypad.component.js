import React, {Component} from 'react';
import {View, TouchableHighlight, Text} from 'react-native';

export default class Keypad extends Component {
    render() {
        const {onNumberPress, onDelete, onNext} = this.props;

        const model = this.getKeypadModel(onNumberPress, onDelete, onNext)
            .map(row => row.map(i => {
                return this.getButton(i.value, i.label, i.callback);
            }));

        const rowStyles = {flex: 1, flexDirection: 'row', justifyContent: 'space-between'};
        return (
            <View style={{flex: 1, marginBottom: 70, marginLeft: 20, marginRight: 20}}>
                <View style={rowStyles} key={'row1'}>
                    {model[0]}
                </View>
                <View style={rowStyles} key={'row2'}>
                    {model[1]}
                </View>
                <View style={rowStyles} key={'row3'}>
                    {model[2]}
                </View>
                <View style={rowStyles} key={'row4'}>
                    {model[3]}
                </View>
            </View>
        );
    }

    getNumberRowModel(dataSet, cb) {
        return dataSet.map(i => this.getSingleModel(i.toString(), i.toString(), cb));
    }

    getSingleModel(value, label, callback) {
        return {
            value, label, callback
        };
    }

    getKeypadModel(onKeyPress, onDelete, onNext) {
        return [
            this.getNumberRowModel([1, 2, 3], onKeyPress),
            this.getNumberRowModel([4, 5, 6], onKeyPress),
            this.getNumberRowModel([7, 8, 9], onKeyPress),
            [
                this.getSingleModel('del', 'Del', onDelete),
                this.getSingleModel(0, 0, onKeyPress),
                this.getSingleModel('next', 'Next', onNext)
            ]
        ]
    }

    getButton(value, label, callback) {
        return (
            <TouchableHighlight
                underlayColor={'#545454'}
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#9c9c9c',
                    borderRadius: 10,
                    margin: 10
                }}
                key={label}
                onPress={() => callback(value)}>
                <Text style={{fontSize: 30}}>{label}</Text>
            </TouchableHighlight>
        );
    }
}
