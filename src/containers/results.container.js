import React, {Component} from 'react';
import {View, ScrollView, Button, Text, TouchableHighlight} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Actions as RouterActions} from 'react-native-router-flux';
import Result from '../components/result.component';
import GameActions from './../actions/game.actions';
import {isAnswerCorrect} from '../helpers/helpers';

class ResultsContainer extends Component {
    render() {
        return (
            <View style={{flex: 1, alignItems: 'center'}}>
                <Text style={{
                    marginTop: 20,
                    color: 'black',
                    textAlign: 'center',
                    fontSize: 35
                }}>Here's how you did</Text>
                <TouchableHighlight
                    underlayColor={'#454545'}
                    style={{
                        padding: 15,
                        backgroundColor: '#aeaeae',
                        borderRadius: 10,
                        marginTop: 25
                    }}
                    onPress={this.startAgain.bind(this)}>
                    <Text style={{fontSize: 25, textAlign: 'center', color: '#343434'}}>Play again</Text>
                </TouchableHighlight>
                {this.renderScore()}
                {this.renderGameTime()}
                <ScrollView style={{
                    marginTop: 10
                }}>{this.renderResults()}</ScrollView>
            </View>
        )
    }

    renderGameTime() {
        const results = this.props.results;
        const gameTime = (results[results.length - 1].endTime - results[0].startTime) / 1000;
        return (<Text style={{
            marginTop: 20,
            color: 'black',
            textAlign: 'center',
            fontSize: 23
        }}>Game time: {gameTime} s</Text>);
    }

    renderScore() {
        const total = this.props.results.length;
        if (total === 0) return null;

        let score = this.getTotalScore();

        return (
            <Text style={{
                marginTop: 20,
                color: 'black',
                textAlign: 'center',
                fontSize: 30
            }}>You got: {score}/{total} ({(score / total) * 100}%)</Text>
        );
    }

    renderResults() {
        const {results} = this.props;
        return results.map((r, i) => (<Result key={i} correct={isAnswerCorrect(r.question, r.answer)} game={r}/>));
    }

    startAgain() {
        this.props.actions.resetGame();
        RouterActions.start();
    }

    getTotalScore() {
        return this.props.results.reduce((acc, i) => {
            if (isAnswerCorrect(i.question, i.answer)) {
                acc++;
            }
            return acc;
        }, 0);
    }
}

const mapStateToProps = (state) => ({
    results: state.results
});

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(GameActions, dispatch)
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ResultsContainer);