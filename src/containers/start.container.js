import React from 'react';
import {Button, View, Text, TouchableHighlight} from 'react-native';
import {Actions as RouterActions} from 'react-native-router-flux';

export default () => {
    return (
        <View style={{flex: 1,  alignItems: 'center'}}>
            <Text style={{
                fontFamily: 'sans-serif-medium',
                marginTop: 20,
                color: 'black',
                textAlign: 'center',
                fontSize: 60
            }}>Numberfy</Text>
            <TouchableHighlight
                underlayColor={'#454545'}
                style={{
                    backgroundColor: '#aeaeae',
                    borderRadius: 10,
                    padding: 15,
                    marginTop: 150
                }}
                onPress={RouterActions.game}>
                <Text style={{fontSize: 35, color: '#343434'}}>Start Game</Text>
            </TouchableHighlight>
        </View>
    );
};

