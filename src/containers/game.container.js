import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import GameActions from './../actions/game.actions';
import {Actions as RouterActions} from 'react-native-router-flux'
import DisplayComponent from './../components/display.component';
import KeypadComponent from '../components/keypad.component';

class GameContainer extends Component {
    componentWillMount() {
        this.props.actions.startGame();
    }

    render() {
        const {question, answer} = this.props;
        const {deletePress} = this.props.actions;
        return (
            <View style={{flex: 1}}>
                <DisplayComponent question={question} answer={answer}/>
                <KeypadComponent
                    onNext={this.onNext.bind(this)} onDelete={deletePress}
                    onNumberPress={this.onNumberPress.bind(this)}/>
            </View>
        );
    }

    onNext() {
        const {endGame, startGame, resetGame} = this.props.actions;
        endGame();
        if (!!this.props.hasNextQuestion) {
            startGame();
        } else {
            RouterActions.results();
        }
    }

    onNumberPress(num) {
        if (this.props.answer.length < 3) {
            this.props.actions.numberPress(num);
        }
    }
}

const mapStateToProps = (state) => ({
    answer: state.current.answer,
    question: state.current.question,
    hasNextQuestion: state.hasNextQuestion
});

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(GameActions, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GameContainer);