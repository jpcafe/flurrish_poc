const numberPress = (number) => ({
    type: 'NUMBER_PRESS',
    payload: {
        number
    }
});

const deletePress = () => ({
    type: 'DELETE_PRESS',
    payload: {}
});

const endGame = () => ({
    type: 'END_GAME',
    payload: {}
});

const startGame = () => ({
    type: 'START_GAME',
    payload: {}
});

const resetGame = () => ({
    type: 'RESET_GAME',
    payload: {}
});

export default {
    numberPress,
    deletePress,
    endGame,
    startGame,
    resetGame
};
