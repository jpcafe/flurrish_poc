import {
    generateNewGame,
    getTime,
    hasNextQuestion,
    updateAnswer,
    deleteAnswer,
    getDefaultGame
} from './../helpers/helpers';

const initialState = {
    current: getDefaultGame(),
    questions: [[3, 8], [4, 5], [6, 8], [1, 9]],
    results: [],
    hasNextQuestion: true,
    questionIndex: 0
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'START_GAME':
            return Object.assign({}, state, {current: generateNewGame(getTime(), state.questions[state.questionIndex])});
        case 'NUMBER_PRESS':
            return Object.assign({}, state, {current: updateAnswer(state.current, action.payload.number)});
        case 'DELETE_PRESS':
            return Object.assign({}, state, {current: deleteAnswer(state.current)});
        case 'END_GAME':
            const game = Object.assign({}, state.current, {endTime: getTime()});
            return Object.assign({}, state, {
                results: [...state.results, game],
                questionIndex: state.questionIndex + 1,
                hasNextQuestion: hasNextQuestion(state.questionIndex + 1, state.questions)
            });
        case 'RESET_GAME':
            return Object.assign({}, state, {
                current: getDefaultGame(),
                hasNextQuestion: true,
                results: [],
                questionIndex: 0
            });
        default:
            return state;
    }
};
