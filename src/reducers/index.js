import {combineReducers} from 'redux';
import GameReducer from './game.reducer';

export default {
    game: GameReducer
};