import React, {Component} from 'react';
import StartContainer from './containers/start.container';
import GameContainer from './containers/game.container';
import ResultsContainer from './containers/results.container';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import reducer from './reducers/game.reducer';
import {Scene, Router} from 'react-native-router-flux';
import createLogger from 'redux-logger';

let middleware = null;

if (__DEV__) {
    middleware = applyMiddleware(createLogger());
}
let store = createStore(reducer, middleware);

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router hideNavBar>
                    <Scene key="start" component={StartContainer} initial={true}/>
                    <Scene key="game" component={GameContainer}/>
                    <Scene key="results" component={ResultsContainer}/>
                </Router>
            </Provider>
        )
    }
}

